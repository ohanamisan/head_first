package section7_Adaptor_Facade

/**
 * 鴨
 */
interface Duck {

    /**
     * ガーガー鳴く
     */
    fun quack()

    /**
     * 飛ぶことができる
     */
    fun fly()
}