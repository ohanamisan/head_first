package section7_Adaptor_Facade

class WildTurkey : Turkey {

    override fun gobble() {
        println("ゴロゴロ")
    }

    override fun fly() {
        println("短い距離を飛んでいます")
    }

}