package section7_Adaptor_Facade

/**
 * 七面鳥
 */
interface Turkey {

    /**
     * ガーガーではなくゴロゴロ鳴く
     */
    fun gobble()

    /**
     * 短い距離しか飛べない
     */
    fun fly()

}