package section7_Adaptor_Facade

class TurkeyAdapter(val turkey: Turkey): Duck {

    override fun quack() = turkey.gobble()

    override fun fly() {
        for (i in 4 downTo 0){
            turkey.fly()
        }
    }
}