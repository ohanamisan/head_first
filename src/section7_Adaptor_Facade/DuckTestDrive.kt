package section7_Adaptor_Facade

fun main(args: Array<String>) {

    val duck = MallardDuck()

    val turkey = WildTurkey()
    val turkeyAdapter = TurkeyAdapter(turkey)

    println("Turkeyの出力")
    turkey.gobble()
    turkey.fly()

    println("Duckの出力")
    duck.quack()
    duck.fly()

    println("TurkeyAdapterの出力")
    testDuck(turkeyAdapter)
}

fun testDuck(duck: Duck){
    duck.quack()
    duck.fly()
}