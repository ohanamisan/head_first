package section7_Adaptor_Facade

class MallardDuck : Duck {

    override fun fly() {
        println("ガーガー")
    }

    override fun quack() {
        println("飛んでいます")
    }
}