package section5_Singleton

class Singleton() {

    private var uniqueInstance: Singleton = Singleton()

    fun getInstance(): Singleton {
        return uniqueInstance
    }

}