package section5_Singleton.chocolate

class ChocolateBoiler() {

    private var empty = true
    private var boiled = false

    private var uniqueInstance = ChocolateBoiler()

    fun getInstance(): ChocolateBoiler{
        return uniqueInstance
    }

    fun fill(){
        if(isEmpty()) {
            empty = false
            boiled = false
            // 牛乳とチョコレートの混合液でボイラを満たす
        }
    }

    fun drain() {
        // 沸騰した牛乳とチョコレートの混合液を空にする
        if(!isEmpty() && isBoiled()) empty = true
    }

    fun boil(){
        // 中身を沸騰させる
        if (!isEmpty() && !isBoiled()) boiled = true
    }

    fun isEmpty() = empty

    fun isBoiled() = boiled
}