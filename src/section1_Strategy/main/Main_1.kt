package section1_Strategy.main

import section1_Strategy.domain.duck.Duck
import section1_Strategy.domain.duck.MallardDuck

fun main(args: Array<String>) {
    val mallard: Duck = MallardDuck()
    with(mallard){
        performQuack()
        performFly()
    }
}