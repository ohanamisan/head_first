package section1_Strategy.main

import section1_Strategy.domain.duck.Duck
import section1_Strategy.domain.duck.MallardDuck
import section1_Strategy.domain.duck.ModelDuck
import section1_Strategy.domain.fly.FlyRocketPowered

fun main(args: Array<String>) {

    val mallard: Duck = MallardDuck()
    with(mallard){
        performQuack()
        performFly()
    }
    val model:Duck = ModelDuck()
    with(model){
        display()
        performFly()
        flyBehavior = FlyRocketPowered()
        performFly()
    }
}