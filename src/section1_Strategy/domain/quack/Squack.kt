package section1_Strategy.domain.quack

class Squack: QuackBehavior {
    override fun quack() = println("キューキュー")
}