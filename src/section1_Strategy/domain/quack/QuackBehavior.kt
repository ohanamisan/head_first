package section1_Strategy.domain.quack

interface QuackBehavior {
    fun quack()
}