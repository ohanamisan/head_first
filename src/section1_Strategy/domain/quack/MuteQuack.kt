package section1_Strategy.domain.quack

class MuteQuack: QuackBehavior {
    override fun quack() = println("鳴けない")
}