package section1_Strategy.domain.quack

class Quack: QuackBehavior {
    override fun quack() = println("ガーガー")
}