package section1_Strategy.domain.duck

import section1_Strategy.domain.fly.FlyWithWings
import section1_Strategy.domain.quack.Quack

class MallardDuck: Duck(FlyWithWings(),
                        Quack()){

    override fun display() = println("マガモの表示")


}