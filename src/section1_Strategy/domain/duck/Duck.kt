package section1_Strategy.domain.duck

import section1_Strategy.domain.fly.FlyBehavior
import section1_Strategy.domain.quack.QuackBehavior

abstract class Duck(var flyBehavior: FlyBehavior,
                    var quackBehavior: QuackBehavior) {

    fun swim() = println("泳いでいる")

    abstract fun display()

    fun performFly() = flyBehavior.fly()

    fun performQuack() = quackBehavior.quack()

}