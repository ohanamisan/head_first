package section1_Strategy.domain.duck

import section1_Strategy.domain.fly.FlyNoWay
import section1_Strategy.domain.quack.Quack

class ModelDuck: Duck(FlyNoWay(),
                        Quack()) {
    override fun display() = println("模型の鴨")
}