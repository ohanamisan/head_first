package section1_Strategy.domain.fly

class FlyWithWings : FlyBehavior{
    override fun fly() = println("翼で空を飛ぶ")
}