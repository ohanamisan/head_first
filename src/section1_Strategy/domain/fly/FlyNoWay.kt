package section1_Strategy.domain.fly

class FlyNoWay: FlyBehavior {
    override fun fly() = println("空を飛べない！")
}