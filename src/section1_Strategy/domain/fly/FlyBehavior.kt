package section1_Strategy.domain.fly

interface FlyBehavior {
    fun fly()
}