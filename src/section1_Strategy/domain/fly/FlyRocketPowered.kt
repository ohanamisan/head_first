package section1_Strategy.domain.fly

class FlyRocketPowered: FlyBehavior {
    override fun fly() = println("ロケットで飛んでいます")
}