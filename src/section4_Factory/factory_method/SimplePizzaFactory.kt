//package section4_Factory.domain
//
//import section4_Factory.factory_method.pizza.*
//
//class SimplePizzaFactory {
//
//    fun createPizza(type: String): Pizza?{
//        return when(type) {
//            "チーズ" -> CheesePizza()
//            "ペパロニ" -> PepperoniPizza()
//            "クラム" -> ClamPizza()
//            "野菜" -> VeggiePizza()
//            else -> null
//        }
//    }
//
//}