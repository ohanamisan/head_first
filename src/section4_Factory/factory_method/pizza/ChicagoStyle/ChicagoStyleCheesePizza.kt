package section4_Factory.factory_method.pizza.ChicagoStyle

import section4_Factory.factory_method.pizza.Pizza

class ChicagoStyleCheesePizza( val _name: String = "シカゴスタイルのディープフィッシュチーズピザ",
                               val _dougth: String = "極厚クラスト生地",
                               val _sauce: String = "プラムトマトソース"
                             ): Pizza(_name,_dougth, _sauce, mutableListOf("刻んだモツァレラチーズ" )){

    override fun cut(){
        println("ピザを四角形に切り分ける")
    }

}
