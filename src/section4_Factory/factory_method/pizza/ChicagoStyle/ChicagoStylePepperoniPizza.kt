package section4_Factory.factory_method.pizza.ChicagoStyle

import section4_Factory.factory_method.pizza.Pizza

class ChicagoStylePepperoniPizza( val _name: String = "シカゴスタイルのペパロニピザ",
                                  val _dougth: String = "極厚クラスト生地",
                                  val _sauce: String = "デスソース"
): Pizza(_name,_dougth, _sauce, mutableListOf("赤唐辛子","青唐辛子","アボガド","マグロ")){

    override fun cut(){
        println("ピザを四角形に切り分ける")
    }
}