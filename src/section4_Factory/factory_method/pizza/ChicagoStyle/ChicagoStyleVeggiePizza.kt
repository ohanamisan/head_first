package section4_Factory.factory_method.pizza.ChicagoStyle

import section4_Factory.factory_method.pizza.Pizza

class ChicagoStyleVeggiePizza( val _name: String = "シカゴスタイルの野菜ピザ",
                               val _dougth: String = "極厚クラスト生地",
                               val _sauce: String = "醤油"
): Pizza(_name,_dougth, _sauce, mutableListOf("サーモン","えんがわ","つぶ貝","うなぎ")){

    override fun cut(){
        println("ピザを四角形に切り分ける")
    }
}