package section4_Factory.factory_method.pizza.NYStyle

import section4_Factory.factory_method.pizza.Pizza

class NYStyleCheesePizza( val _name: String = "ニューヨークスタイルのソース&チーズピザ",
                          val _dougth: String = "薄いクラスト生地",
                          val _sauce: String = "マリナラソース"
                         ): Pizza(_name,_dougth, _sauce, mutableListOf("粉レッジャーノチーズ" ))