package section4_Factory.factory_method.pizza.NYStyle

import section4_Factory.factory_method.pizza.Pizza

class NYStyleClamPizza( val _name: String = "ニューヨークスタイルのクラムピザ",
                        val _dougth: String = "薄いクラスト生地",
                        val _sauce: String = "クラムチャウダー"
): Pizza(_name,_dougth, _sauce, mutableListOf("さけるチーズ","イカ","冷やしトマト" ))