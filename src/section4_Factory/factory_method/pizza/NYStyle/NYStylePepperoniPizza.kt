package section4_Factory.factory_method.pizza.NYStyle

import section4_Factory.factory_method.pizza.Pizza

class NYStylePepperoniPizza( val _name: String = "ニューヨークスタイルのペパロニピザ",
                             val _dougth: String = "薄いクラスト生地",
                             val _sauce: String = "ペパロニソース"
): Pizza(_name,_dougth, _sauce, mutableListOf("とろけるチーズ" ))