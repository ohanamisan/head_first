package section4_Factory.factory_method.pizza.NYStyle

import section4_Factory.factory_method.pizza.Pizza

class NYStyleVeggiePizza( val _name: String = "ニューヨークスタイルの野菜ピザ",
                          val _dougth: String = "薄いクラスト生地",
                          val _sauce: String = "トマトソース"
): Pizza(_name,_dougth, _sauce, mutableListOf("にんにく","オクラ","じゃがいも","ウインナー" ))