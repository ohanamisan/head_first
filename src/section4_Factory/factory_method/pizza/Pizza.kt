package section4_Factory.factory_method.pizza

abstract class Pizza( val name: String,
                      val dough: String,
                      val sauce: String,
                      val toppings: MutableList<String>) {

    fun prepare(){
        println("${name}を下処理")
        println("生地をこねる...")
        println("ソースを追加...")
        println("トッピングを追加 : ")
        toppings.forEach{ println(it) }
    }

    fun bake(){
        println("350度で25分間焼く")
    }

    open fun cut(){
        println("ピザを扇形に切り分ける")
    }

    fun box(){
        println("箱にピザを詰める")
    }

}