package section4_Factory.factory_method.store

import section4_Factory.factory_method.pizza.NYStyle.NYStyleCheesePizza
import section4_Factory.factory_method.pizza.NYStyle.NYStyleClamPizza
import section4_Factory.factory_method.pizza.NYStyle.NYStylePepperoniPizza
import section4_Factory.factory_method.pizza.NYStyle.NYStyleVeggiePizza
import section4_Factory.factory_method.pizza.Pizza

class NYPizzaStore: PizzaStore() {

    override fun createPizza(type: String): Pizza? {
        return when(type){
            "チーズ" -> NYStyleCheesePizza()
            "クラム" -> NYStyleClamPizza()
            "ペパロニ" -> NYStylePepperoniPizza()
            "野菜" -> NYStyleVeggiePizza()
            else -> null
        }
    }
}