package section4_Factory.factory_method.store

import section4_Factory.factory_method.pizza.ChicagoStyle.ChicagoStyleCheesePizza
import section4_Factory.factory_method.pizza.ChicagoStyle.ChicagoStyleClamPizza
import section4_Factory.factory_method.pizza.ChicagoStyle.ChicagoStylePepperoniPizza
import section4_Factory.factory_method.pizza.ChicagoStyle.ChicagoStyleVeggiePizza
import section4_Factory.factory_method.pizza.Pizza

class ChicagoPizzaStore: PizzaStore() {

    override fun createPizza(type: String): Pizza? {
        return when(type){
            "チーズ" -> ChicagoStyleCheesePizza()
            "クラム" -> ChicagoStyleClamPizza()
            "ペパロニ" -> ChicagoStylePepperoniPizza()
            "野菜" -> ChicagoStyleVeggiePizza()
            else -> null
        }
    }
}