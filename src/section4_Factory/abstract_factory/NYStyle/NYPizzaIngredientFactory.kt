package section4_Factory.abstract_factory.NYStyle

import section4_Factory.abstract_factory.PizzaIngredientFactory
import section4_Factory.abstract_factory.cheese.Cheese
import section4_Factory.abstract_factory.cheese.ReggianoCheese
import section4_Factory.abstract_factory.clams.Clams
import section4_Factory.abstract_factory.clams.FreshClams
import section4_Factory.abstract_factory.dough.Dough
import section4_Factory.abstract_factory.dough.ThinCrustDough
import section4_Factory.abstract_factory.pepproni.Pepperoni
import section4_Factory.abstract_factory.pepproni.SlicedPepperoni
import section4_Factory.abstract_factory.sauce.MarianaSauce
import section4_Factory.abstract_factory.sauce.Sauce
import section4_Factory.abstract_factory.veggies.Mushroom
import section4_Factory.abstract_factory.veggies.Onion
import section4_Factory.abstract_factory.veggies.Veggies

class NYPizzaIngredientFactory : PizzaIngredientFactory {

    override fun createDough(): Dough = ThinCrustDough()

    override fun createSauce(): Sauce =  MarianaSauce()

    override fun createCheese(): Cheese = ReggianoCheese()

    override fun createVeggies(): Array<Veggies> = arrayOf(Onion(),Mushroom())

    override fun createPepperoni(): Pepperoni = SlicedPepperoni()

    override fun createClams(): Clams = FreshClams()
}