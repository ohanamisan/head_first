package section4_Factory.abstract_factory.store

import section4_Factory.abstract_factory.NYStyle.NYPizzaIngredientFactory
import section4_Factory.abstract_factory.PizzaIngredientFactory
import section4_Factory.abstract_factory.pizza.CheezePizza
import section4_Factory.abstract_factory.pizza.ClamPizza
import section4_Factory.abstract_factory.pizza.Pizza

class NYPizzaStore: PizzaStore() {

    val ingredientFactory: PizzaIngredientFactory = NYPizzaIngredientFactory()

    override fun cretePizza(item:String):Pizza? {
        return when(item){
            "チーズ" -> CheezePizza(ingredientFactory,item)
            "クラム" -> ClamPizza(ingredientFactory,item)
            else -> null
        }
    }
}