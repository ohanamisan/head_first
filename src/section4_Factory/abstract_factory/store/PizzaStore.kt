package section4_Factory.abstract_factory.store

import section4_Factory.abstract_factory.pizza.Pizza

abstract class PizzaStore {
    abstract fun cretePizza(item:String):Pizza?
}