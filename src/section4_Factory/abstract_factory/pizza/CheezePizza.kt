package section4_Factory.abstract_factory.pizza

import section4_Factory.abstract_factory.PizzaIngredientFactory

class CheezePizza(ingredientFactory: PizzaIngredientFactory, name:String): Pizza(name,ingredientFactory) {

    override fun prepare() {
        println("${name}を下処理")
        dough
        sauce
        cheese
    }
}