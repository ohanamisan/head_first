package section4_Factory.abstract_factory.pizza

import section4_Factory.abstract_factory.PizzaIngredientFactory
import section4_Factory.abstract_factory.cheese.Cheese
import section4_Factory.abstract_factory.clams.Clams
import section4_Factory.abstract_factory.dough.Dough
import section4_Factory.abstract_factory.pepproni.Pepperoni
import section4_Factory.abstract_factory.sauce.Sauce
import section4_Factory.abstract_factory.veggies.Veggies

abstract class Pizza(val name:String,val ingredientFactory: PizzaIngredientFactory){


    val cheese: Cheese = ingredientFactory.createCheese()
    val dough: Dough = ingredientFactory.createDough()
    val clams: Clams = ingredientFactory.createClams()
    val pepperoni: Pepperoni = ingredientFactory.createPepperoni()
    val sauce: Sauce = ingredientFactory.createSauce()
    val veggies: Array<Veggies> = ingredientFactory.createVeggies()


    abstract fun prepare()

    fun bake() = println("350度で25分間焼く")

    fun cut() = println("ピザを扇状に切り分ける")

    fun box() = println("PizzaStoreの正式な箱にピザを入れる")

}