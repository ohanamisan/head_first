package section4_Factory.abstract_factory.ChicagoStyle

import section4_Factory.abstract_factory.PizzaIngredientFactory
import section4_Factory.abstract_factory.cheese.Cheese
import section4_Factory.abstract_factory.clams.Clams
import section4_Factory.abstract_factory.dough.Dough
import section4_Factory.abstract_factory.pepproni.Pepperoni
import section4_Factory.abstract_factory.sauce.Sauce
import section4_Factory.abstract_factory.veggies.Veggies

class ChicagoPizzaIngredientFactory:PizzaIngredientFactory {

    override fun createDough(): Dough {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createSauce(): Sauce {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createCheese(): Cheese {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createVeggies(): Array<Veggies> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createPepperoni(): Pepperoni {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createClams(): Clams {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}