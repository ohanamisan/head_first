package section4_Factory.abstract_factory

import section4_Factory.abstract_factory.cheese.Cheese
import section4_Factory.abstract_factory.clams.Clams
import section4_Factory.abstract_factory.dough.Dough
import section4_Factory.abstract_factory.pepproni.Pepperoni
import section4_Factory.abstract_factory.sauce.Sauce
import section4_Factory.abstract_factory.veggies.Veggies

interface PizzaIngredientFactory {

    fun createDough(): Dough

    fun createSauce(): Sauce

    fun createCheese(): Cheese

    fun createVeggies():Array<Veggies>

    fun createPepperoni(): Pepperoni

    fun createClams(): Clams

}