package section4_Factory.main

import section4_Factory.factory_method.store.ChicagoPizzaStore
import section4_Factory.factory_method.store.NYPizzaStore

fun main(args: Array<String>) {

    val nyStore = NYPizzaStore()
    nyStore.orderPizza("チーズ")
    nyStore.orderPizza("クラム")
    nyStore.orderPizza("ペパロニ")
    nyStore.orderPizza("野菜")

    val chicagoStore = ChicagoPizzaStore()
    chicagoStore.orderPizza("チーズ")
    chicagoStore.orderPizza("クラム")
    chicagoStore.orderPizza("ペパロニ")
    chicagoStore.orderPizza("野菜")

}