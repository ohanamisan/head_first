package section3_Decorator.domain.coffee

open abstract class Beverage(open val description: String = "不明な飲み物") {
    abstract fun cost():Double
}