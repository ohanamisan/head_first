package section3_Decorator.domain.coffee

import section3_Decorator.domain.coffee.Beverage

class HouseBlend: Beverage() {

    override val description: String
        get() = "ハウスブレンド"

    override fun cost(): Double {
        return 0.89
    }
}