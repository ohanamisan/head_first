package section3_Decorator.domain.coffee

import section3_Decorator.domain.coffee.Beverage

class DarkRoast: Beverage() {

    override val description: String
        get() = "ダークロースト"

    override fun cost(): Double {
        return 0.99
    }
}