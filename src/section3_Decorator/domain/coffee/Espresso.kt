package section3_Decorator.domain.coffee

import section3_Decorator.domain.coffee.Beverage

class Espresso : Beverage() {

    override val description: String
        get() = "エスプレッソ"

    override fun cost(): Double {
        return 1.99
    }
}