package section3_Decorator.domain.coffee

import section3_Decorator.domain.coffee.Beverage

class Decaf: Beverage() {

    override val description: String
        get() = "カフェイン抜き"

    override fun cost(): Double {
        return 1.05
    }
}