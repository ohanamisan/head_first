package section3_Decorator.domain.topping

import section3_Decorator.domain.coffee.Beverage
import section3_Decorator.domain.topping.CondimentDecorator

class Mocha(val _beverage: Beverage): CondimentDecorator() {

    var beverage: Beverage = this._beverage

    override val description: String
        get() = "${this.beverage.description}、モカ"

    override fun cost(): Double {
        return 0.20 + this.beverage.cost()
    }
}