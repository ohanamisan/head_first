package section3_Decorator.domain.topping

import section3_Decorator.domain.coffee.Beverage
import section3_Decorator.domain.topping.CondimentDecorator

class Soy(val _beverage: Beverage): CondimentDecorator() {

    var beverage: Beverage = this._beverage

    override val description: String
        get() = "${this.beverage.description}、豆乳"

    override fun cost(): Double {
        return 0.15 + this.beverage.cost()
    }
}