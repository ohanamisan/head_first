package section3_Decorator.domain.topping

import section3_Decorator.domain.coffee.Beverage
import section3_Decorator.domain.topping.CondimentDecorator

class Whip(val _beverage: Beverage): CondimentDecorator() {

    var beverage: Beverage = this._beverage

    override val description: String
        get() = "${this.beverage.description}、ホイップ"

    override fun cost(): Double {
        return 0.10 + this.beverage.cost()
    }
}