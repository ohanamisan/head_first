package section3_Decorator.main

import section3_Decorator.domain.coffee.Beverage
import section3_Decorator.domain.coffee.DarkRoast
import section3_Decorator.domain.coffee.Espresso
import section3_Decorator.domain.coffee.HouseBlend
import section3_Decorator.domain.topping.Mocha
import section3_Decorator.domain.topping.Soy
import section3_Decorator.domain.topping.Whip

fun main(args: Array<String>) {

    var beverage: Beverage = Espresso()

    println(beverage.description)
    println(beverage.cost())

    var beverage2: Beverage = DarkRoast()

    beverage2 = Mocha(beverage2)
    beverage2 = Mocha(beverage2)
    beverage2 = Whip(beverage2)

    println(beverage2.description)
    println(beverage2.cost())

    var beverage3: Beverage = Whip(Mocha(Soy(HouseBlend())))

    println(beverage3.description)
    println(beverage3.cost())

}