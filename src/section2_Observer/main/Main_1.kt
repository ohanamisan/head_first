package section2_Observer.main

import section2_Observer.domain.Display.CurrentConditionsDisplay
import section2_Observer.domain.WeatherData

fun main(args: Array<String>) {

    val weatherData: WeatherData = WeatherData()

    val currentConditionsDisplay: CurrentConditionsDisplay =
            CurrentConditionsDisplay(weatherData)

    weatherData.setMeasurements(27.0, 65.0, 30.4)
    weatherData.setMeasurements(28.0, 70.0, 29.2)
    weatherData.setMeasurements(26.0, 90.0, 29.2)


}