package section2_Observer.main

import section2_Observer.domain.CurrentConditionsDisplayByUtil
import section2_Observer.domain.Display.CurrentConditionsDisplay
import section2_Observer.domain.WeatherData
import section2_Observer.domain.WeatherDataByUtil

fun main(args: Array<String>) {
    val weatherData: WeatherDataByUtil = WeatherDataByUtil()

    val currentConditionsDisplay: CurrentConditionsDisplayByUtil =
            CurrentConditionsDisplayByUtil(weatherData)

    weatherData.setMeasurements(27.0, 65.0, 30.4)
    weatherData.setMeasurements(28.0, 70.0, 29.2)
    weatherData.setMeasurements(26.0, 90.0, 29.2)


}