package section2_Observer.domain

import java.util.*

class WeatherDataByUtil: Observable(){

    var temperature: Double = 0.0
    var humidity: Double = 0.0
    var pressure: Double = 0.0

    fun measurementsChanged(){
        setChanged()
        notifyObservers()
    }

    fun setMeasurements(temperature: Double, humidity: Double, pressure: Double){
        this.temperature = temperature
        this.humidity = humidity
        this.pressure = pressure
        measurementsChanged()
    }

}