package section2_Observer.domain.Observer

typealias Temperature = Double
typealias Humidity = Double
typealias Pressure = Double

interface Observer {
    fun update(temp:Temperature, humidity: Humidity, pressure: Pressure)
}