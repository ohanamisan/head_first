package section2_Observer.domain.Observer

class ConcreteObserver: Observer {

    override fun update(temp: Double, humidity: Double, pressure: Double) {}


}