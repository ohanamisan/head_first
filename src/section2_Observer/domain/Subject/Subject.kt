package section2_Observer.domain.Subject

import section2_Observer.domain.Observer.Observer

interface Subject {

    fun registerObserver(observer: Observer){}

    fun removeObserver(observer: Observer){}

    fun notifyObservers(){}

}