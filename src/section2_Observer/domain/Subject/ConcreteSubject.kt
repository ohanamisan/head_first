package section2_Observer.domain.Subject

import section2_Observer.domain.Observer.Observer

class ConcreteSubject: Subject {

    override fun registerObserver(observer: Observer){}

    override fun removeObserver(observer: Observer) {}

    override fun notifyObservers() {}

    fun getState(){}

    fun setState(){}


}