package section2_Observer.domain.Display

interface DisplayElement {
    fun display()
}