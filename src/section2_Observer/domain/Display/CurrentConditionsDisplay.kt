package section2_Observer.domain.Display

import section2_Observer.domain.Observer.Observer
import section2_Observer.domain.Subject.Subject

class CurrentConditionsDisplay : Observer, DisplayElement {

    var temperature: Double = 0.0
    var humidity: Double = 0.0
    private val weatherData: Subject

    constructor(weatherData: Subject){
        this.weatherData = weatherData
        weatherData.registerObserver(this)
    }

    override fun update(temperature: Double, humidity: Double, pressure: Double) {
        this.temperature = temperature
        this.humidity = humidity
        display()
    }

    override fun display() {
        println("""
            現在の気象状況
            温度：${temperature}度
            湿度：${humidity}%
            """)
    }


}