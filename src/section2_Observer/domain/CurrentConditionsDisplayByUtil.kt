package section2_Observer.domain

import section2_Observer.domain.Display.DisplayElement
import java.util.*

class CurrentConditionsDisplayByUtil(private val observable: Observable,
                                     var temperature: Double = 0.0,
                                     var humidity: Double = 0.0) :
        Observer, DisplayElement {

    override fun update(obs: Observable, arg: Any?) {
        if (obs is WeatherDataByUtil){
            var weatherData: WeatherDataByUtil = obs
            this.temperature = weatherData.temperature
            this.humidity = weatherData.humidity
            display()
        }
    }

    override fun display() = println("""
            現在の気象状況
            温度：${temperature}度
            湿度：${humidity}%
        """)


}