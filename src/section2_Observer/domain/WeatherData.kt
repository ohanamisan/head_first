package section2_Observer.domain

import section2_Observer.domain.Observer.Observer
import section2_Observer.domain.Subject.Subject

class WeatherData: Subject {

    var observers: MutableList<Observer> = mutableListOf()
    var temperature: Double = 0.0
    var humidity: Double = 0.0
    var pressure: Double = 0.0


    /**
     * オブザーバの新規登録
     */
    override fun registerObserver(observer: Observer) {
        observers.add(observer)
    }

    /**
     * オブザーバの登録解除
     */
    override fun removeObserver(observer: Observer) {
        var i: Int = observers.indexOf(observer)
        if (i >= 0){
            observers.removeAt(i)
        }
    }

    /**
     * 全てのオブザーバに状態を通知
     */
    override fun notifyObservers() {
        for (o in observers){
            o.update(this.temperature, this.humidity, this.pressure)
        }
    }

    /**
     * このメソッドは、気象観測所が
     * 更新されるたびに呼び出されます
     */
    fun measurementsChanged(){
        notifyObservers()
    }

    fun setMeasurements(temperature: Double, humidity: Double, pressure: Double){
        this.temperature = temperature
        this.humidity = humidity
        this.pressure = pressure
        measurementsChanged()
    }
}